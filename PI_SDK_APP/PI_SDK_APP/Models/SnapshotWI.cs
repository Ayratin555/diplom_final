﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PI_SDK_APP.Models
{
    public class SnapshotWI
    {
        /// <summary>
        /// Идентификатор атрибута
        /// </summary>
        public string ID { get; set; }
        /// <summary>
        /// Наименование атрибута
        /// </summary>
        public string NAME { get; set; }
        /// <summary>
        /// DateTime значения
        /// </summary>
        public string DATE { get; set; }
        /// <summary>
        /// Значение
        /// </summary>
        public string VALUE { get; set; }
        /// <summary>
        /// Состояние
        /// </summary>
        public string STATE { get; set; }
    }
}