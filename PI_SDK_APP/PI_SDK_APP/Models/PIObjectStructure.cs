﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PIObjectStructure
{
    public class Links
    {
        public string Self { get; set; }
        public string Attributes { get; set; }
        public string Elements { get; set; }
        public string Database { get; set; }
        public string Parent { get; set; }
    }

    public class Item
    {
        public string WebId { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Path { get; set; }
        public string TemplateName { get; set; }
        public List<String> CategoryNames { get; set; }
        public Links Links { get; set; }
    }

    public class PIObjectList
    {
        public Links Links { get; set; }
        public List<Item> Items { get; set; }
    }
}