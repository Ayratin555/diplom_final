﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PIAttributeStructure
{

    public class Links
    {
        public string Self { get; set; }
        public string Attributes { get; set; }
        public string Element { get; set; }
        public string Value { get; set; }
        public string InterpolatedData { get; set; }
        public string RecordedData { get; set; }
        public string PlotData { get; set; }
        public string SummaryData { get; set; }
    }

    public class PIAttribute
    {
        public string WebId { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Path { get; set; }
        public string DefaultUnitsName { get; set; }
        public string DataReferencePlugIn { get; set; }
        public string ConfigString { get; set; }
        public bool IsConfigurationItem { get; set; }
        public List<object> CategoryNames { get; set; }
        public Links Links { get; set; }
    }

    public class PIAttributeList
    {
        public Links Links { get; set; }
        public List<PIAttribute> Items { get; set; }
    }
}