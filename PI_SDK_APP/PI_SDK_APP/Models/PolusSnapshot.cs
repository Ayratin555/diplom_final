﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PI_SDK_APP.Models
{
    public class PolusSnapshot
    {
        public List<SnapshotWI> DATA { get; set; }
        public string ERROR { get; set; }
    }
}