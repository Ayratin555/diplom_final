﻿using OSIsoft.AF;
using OSIsoft.AF.Asset;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PI_SDK_APP.Models
{
    public class PairElement_Attribute
    {
        public PairElement_Attribute(PISystem System, string Value)
        {
            string[] pars = Value.Split('~');
            if (pars.Length != 2)
                throw new Exception(string.Format("Некорректный формат элемента '{0}'", Value));

            _element = AFElement.FindElement(System, new Guid(pars[0]));
            if (_element == null)
                throw new Exception(string.Format("Элемент '{0}' не найден", pars[0]));

            _attribute = AFAttribute.FindAttribute(_element, new Guid(pars[1]));
            if (_attribute == null)
                throw new Exception(string.Format("Атрибут '{0}' не найден", pars[1]));

            _attributeValue = _attribute.Attributes["Value"];
            if (_attributeValue == null)
                throw new Exception(string.Format("Атрибут '{0}' не имеет дочернего атрибута 'Value'", pars[1]));

            if (_attributeValue == null)
                throw new Exception(string.Format("Атрибут '{0}' не найден", _attributeValue));
        }

        AFElement _element = null;
        public AFElement Element
        {
            get
            {
                return _element;
            }
        }

        AFAttribute _attribute = null;
        public AFAttribute Attribute
        {
            get
            {
                return _attribute;
            }
        }

        AFAttribute _attributeValue = null;
        public AFAttribute AttributeValue
        {
            get
            {
                return _attributeValue;
            }
        }
    }
}