﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using PI_SDK_APP.Models;
using PIAttributeStructure;
using System.Diagnostics;

namespace PI_SDK_APP.Services
{
    public class ValueRetriever
    {
        private const String sourcePathTest = "\\\\PISERVER\\Tatneft\\ТестБадыков\\Ашальчинское%20месторождение";
        private const String sourcePath = "\\\\PIAF\\Tatneft\\ОАО «Татнефть»\\НГДУ «Нурлатнефть»\\Ашальчинское месторождение";
        private const String serverAddress = "https://10.1.4.52/piwebapi/attributes/?path=";



       

        public double retrieveValue(String wellPath, String parameterPath)
        {

            String pathToValue = getPathToValue(wellPath, parameterPath);
            double value = getValueByPath(pathToValue);
            return value;


        }

        public double getValueByPath(String pathToValue)
        {

            Stopwatch stopWatch = Stopwatch.StartNew();

            WebClient proper = getProperWebClient();
            string jsonWithTagAttribute = proper.DownloadString(pathToValue);

            stopWatch.Stop();
            String time = "Получение аттрибута : " + stopWatch.ElapsedMilliseconds.ToString() + System.Environment.NewLine;
            System.Diagnostics.Debug.WriteLine(time);
            System.IO.File.AppendAllText(@"C:\Users\Ayrat\Documents\log1.txt", time);


            stopWatch = Stopwatch.StartNew();

            string tagValueAttributeAddr = getValueAddress(jsonWithTagAttribute);

            stopWatch.Stop();
            time = "Получение адреса на значение : " + stopWatch.ElapsedMilliseconds.ToString() + System.Environment.NewLine;
            System.Diagnostics.Debug.WriteLine(time);
            System.IO.File.AppendAllText(@"C:\Users\Ayrat\Documents\log1.txt", time);

            stopWatch = Stopwatch.StartNew();

            string jsonWithValue = proper.DownloadString(tagValueAttributeAddr);


            double value = parseValue(jsonWithValue);

            stopWatch.Stop();
            time = "Получение значения : " + stopWatch.ElapsedMilliseconds.ToString() + System.Environment.NewLine;
            System.Diagnostics.Debug.WriteLine(time);
            System.IO.File.AppendAllText(@"C:\Users\Ayrat\Documents\log1.txt", time);

            
            return value;
        }

        public string getValueAddress(string jsonWithTagAttribute)
        {
            PIAttribute withValue = new PIAttribute();
            withValue = JsonConvert.DeserializeObject<PIAttribute>(jsonWithTagAttribute);


            string valueAddress = withValue.Links.Value;

            return valueAddress;

        }

        public double parseValue(string jsonWithValue)
        {

            dynamic obj = JObject.Parse(jsonWithValue);
            double value = (double)obj.Value;
            return value;

        }
      



        String getPathToValue(String wellPath, String parameterPath)
        {
            String well = parseWell(wellPath);
            String device = parseDevice(parameterPath);
            String tag = wellPath + '.' + well + parameterPath.Substring(1);
            String delimiter = "\\";
            String pathToValue = serverAddress + sourcePath + delimiter + well + delimiter + device + delimiter + tag + "|Value";
            return pathToValue;


        }

        public String parseWell(String wellPath)
        {
            char delimiter = '.';
            string[] tagHierarchyPoints = wellPath.Split(delimiter);
            string well = tagHierarchyPoints[tagHierarchyPoints.Length - 1];
            return well;
        }

        public String parseDevice(String parameterPath)
        {
            char delimiterForTag = '.';
            char delimiterForDevice = '_';
            string[] tagHierarchyPoints = parameterPath.Split(delimiterForTag);
            string wellWithDevice = tagHierarchyPoints[tagHierarchyPoints.Length - 2];
            string[] wellWithDevicePoints = wellWithDevice.Split(delimiterForDevice);
            string device = wellWithDevicePoints[wellWithDevicePoints.Length - 1];
            string deviceWithProperDevice = firstLetterUpperCase(device);
            return deviceWithProperDevice;
        }

        public String firstLetterUpperCase(string word)
        {

            if (string.IsNullOrEmpty(word))
            {
                return string.Empty;
            }
            string wordWithoutFirstLetter = word.Substring(1).ToLower();
            return char.ToUpper(word[0]) + wordWithoutFirstLetter;

        }

        public WebClient getProperWebClient()
        {
            WebClient client = new WebClient();
            IgnoreAllCertificates();
            client.Encoding = System.Text.Encoding.UTF8;
            return client;

        }

        public void IgnoreAllCertificates()
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
        }
    }
}