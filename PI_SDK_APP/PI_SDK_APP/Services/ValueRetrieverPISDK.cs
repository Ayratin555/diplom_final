﻿using OSIsoft.AF;
using OSIsoft.AF.Asset;
using OSIsoft.AF.Time;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Diagnostics;
using System.Web.Mvc;
using PI_SDK_APP.Models;


namespace PI_SDK_APP.Services
{
    public class ValueRetrieverPISDK
    {
        private string PIServer { get { return System.Web.Configuration.WebConfigurationManager.AppSettings["PIServer"]; } }
        private string PIUserDomain { get { return System.Web.Configuration.WebConfigurationManager.AppSettings["PIUserDomain"]; } }
        private string PIUser { get { return System.Web.Configuration.WebConfigurationManager.AppSettings["PIUser"]; } }
        private string PIPassword { get { return System.Web.Configuration.WebConfigurationManager.AppSettings["PIPassword"]; } }
        private string PIDB { get { return System.Web.Configuration.WebConfigurationManager.AppSettings["PIDB"]; } }
        private string RootTree { get { return System.Web.Configuration.WebConfigurationManager.AppSettings["RootTree"]; } }
        private String wellPath;
        private String parameterPath;
        private double value;

      

        public String retrieveValue(String wellPath, String parameterPath)
        {
            Stopwatch stopWatch = Stopwatch.StartNew();


            this.parameterPath = parameterPath;
            this.wellPath = wellPath;
            string well = parseWell(wellPath);
            string device = parseDevice(parameterPath);
            String tag = wellPath + '.' + well + parameterPath.Substring(1);
            String delimiter = "\\";
            String delimiter1 = "\\\\";
            String pathToValue = RootTree + delimiter + well + delimiter + device + delimiter + tag + "|Value";
            AFDatabase db = PIDatabaseActive;

            AFAttribute attr = AFObject.FindObject(pathToValue) as AFAttribute;

            AFValue d = attr.GetValue();




            stopWatch.Stop();
            String time = "Получение значения : " + stopWatch.ElapsedMilliseconds.ToString() + System.Environment.NewLine;
            System.Diagnostics.Debug.WriteLine(time);
            System.IO.File.AppendAllText(@"C:\Users\Ayrat\Documents\log.txt", time);
           


            return d.Value.ToString();
        }

        public String parseWell(String wellPath)
        {
            char delimiter = '.';
            string[] tagHierarchyPoints = wellPath.Split(delimiter);
            string well = tagHierarchyPoints[tagHierarchyPoints.Length - 1];
            return well;
        }

        public String parseDevice(String parameterPath)
        {
            char delimiterForTag = '.';
            char delimiterForDevice = '_';
            string[] tagHierarchyPoints = parameterPath.Split(delimiterForTag);
            string wellWithDevice = tagHierarchyPoints[tagHierarchyPoints.Length - 2];
            string[] wellWithDevicePoints = wellWithDevice.Split(delimiterForDevice);
            string device = wellWithDevicePoints[wellWithDevicePoints.Length - 1];
            string deviceWithProperDevice = firstLetterUpperCase(device);
            return deviceWithProperDevice;
        }

        public String firstLetterUpperCase(string word)
        {

            if (string.IsNullOrEmpty(word))
            {
                return string.Empty;
            }
            string wordWithoutFirstLetter = word.Substring(1).ToLower();
            return char.ToUpper(word[0]) + wordWithoutFirstLetter;

        }

        private int MaxRecordCount
        {
            get
            {
                try
                {
                    return Convert.ToInt32(System.Web.Configuration.WebConfigurationManager.AppSettings["MaxRecordCount"]);
                }
                catch
                {
                    return 0;
                }
            }
        }

        public PISystem PISystemActive
        {
            get
            {
                try
                {
                    Stopwatch stopWatch = Stopwatch.StartNew(); 
                    PISystem _PISystem = (PISystem)HttpRuntime.Cache["PISystem"];
                    if (_PISystem == null)
                    {
                        PISystems systems = new PISystems();

                        _PISystem = string.IsNullOrEmpty(PIServer) ? systems.DefaultPISystem : systems[PIServer];

                        NetworkCredential R = new NetworkCredential();
                        if (!string.IsNullOrEmpty(PIUserDomain))
                            R.Domain = PIUserDomain;
                        if (!string.IsNullOrEmpty(PIUser))
                            R.UserName = PIUser;
                        if (!string.IsNullOrEmpty(PIPassword))
                            R.Password = PIPassword;

                        if (_PISystem.ConnectionInfo.IsConnected) //
                            _PISystem.Disconnect(); //
                        _PISystem.Connect(R);
                        HttpRuntime.Cache["PISystem"] = _PISystem;
                    }
                    if (!_PISystem.ConnectionInfo.IsConnected)
                    {
                        _PISystem.Connect();
                    }
                    stopWatch.Stop();
                    String time = "Получение PISystemActive : " + stopWatch.ElapsedMilliseconds.ToString() + System.Environment.NewLine;
                    System.Diagnostics.Debug.WriteLine(time);
                    System.IO.File.AppendAllText(@"C:\Users\Ayrat\Documents\log.txt", time);
                    return _PISystem;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public AFDatabase PIDatabaseActive
        {
            
            
            
            get
            {

                //----------------------------------------------------------------------------

                Stopwatch stopWatch = Stopwatch.StartNew();

                //----------------------------------------------------------------------------

                AFDatabases AFBDlist = PISystemActive.Databases;
                AFDatabase AFDatabase = PISystemActive.Databases[PIDB];
                AFDatabase.Refresh();

                //----------------------------------------------------------------------------


                
                stopWatch.Stop();
                String time = "Получение PIDatabaseActive : " + stopWatch.ElapsedMilliseconds.ToString() + System.Environment.NewLine;
                System.Diagnostics.Debug.WriteLine(time);
                System.IO.File.AppendAllText(@"C:\Users\Ayrat\Documents\log.txt", time);


                //----------------------------------------------------------------------------------


                return AFDatabase;
            }
        }


        public PolusSnapshot GetSnapshots(string objectId, List<string> techParams)
        {
            try
            {
                PolusSnapshot result = new PolusSnapshot();
                result.DATA = new List<SnapshotWI>();
                try
                {
                    if (string.IsNullOrEmpty(objectId))
                        throw new Exception("Не передан обязательный элемент objectId");

                    AFElement element = AFElement.FindElement(PISystemActive, new Guid(objectId));
                    if (element == null)
                        throw new Exception(string.Format("Элемент '{0}' не найден", objectId));

                    foreach (AFAttribute attribute in element.Attributes)
                    {
                        Boolean BadState = false;
                        if (techParams == null || techParams.Contains(attribute.Name))
                        {
                            AFAttribute attributeValue = attribute.Attributes["Value"];
                            if (attributeValue != null)
                            {
                                OSIsoft.AF.PI.PIPoint point = attributeValue.DataReference.PIPoint;
                                if (point != null)
                                {
                                    AFValue val = point.Snapshot();

                                    OSIsoft.AF.UnitsOfMeasure.UOM uom = val.UOM;  // Mega UOM ; + вынести в репозитарий

                                    result.DATA.Add(new SnapshotWI()
                                    {
                                        ID = attribute.ID.ToString(),
                                        NAME = attribute.Name,
                                        DATE = val.Timestamp.LocalTime.ToString("dd.MM.yyyy HH:mm:ss"),
                                        VALUE = val.Value.ToString()
                                    });
                                }
                            }
                            else
                                BadState = true;
                        }
                        else
                            BadState = true;

                        if (BadState)
                        {
                            result.DATA.Add(new SnapshotWI()
                            {
                                ID = attribute.ID.ToString(),
                                NAME = attribute.Name,
                                DATE = DateTime.MinValue.ToString("dd.MM.yyyy HH:mm:ss"),
                                VALUE = "",
                                STATE = "not found"
                            });
                        }

                    }
                }
                catch (Exception ex)
                {
                    result = new PolusSnapshot();
                    result.ERROR = ex.Message;
                }

                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


    }
}