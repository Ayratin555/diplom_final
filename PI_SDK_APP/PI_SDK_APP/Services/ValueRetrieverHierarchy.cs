﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PIAttributeStructure;
using PIObjectStructure;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web;

namespace PI_SDK_APP.Services
{
    public class ValueRetrieverHierarchy
    {


        private const String ServerAddrTest = "https://10.1.4.52/piwebapi/elements/E0Nq8LBW-gTUefGQG57TrJVAJnh8ray15BGyD1C3w-Vt5gUElTRVJWRVJcVEFUTkVGVFzQotCV0KHQotCR0JDQlNCr0JrQntCSXNCQ0KjQkNCb0KzQp9CY0J3QodCa0J7QlSDQnNCV0KHQotCe0KDQntCW0JTQldCd0JjQlQ/elements";
        private const String ServerAddr = "https://10.1.4.52/piwebapi/elements/E0enFAFaZwuEGpffzgiGOC7gcjdLUH6g5BGAxAAiZAxY5gUElBRlxUQVRORUZUXNCe0JDQniDCq9Ci0JDQotCd0JXQpNCi0KzCu1zQndCT0JTQoyDCq9Cd0KPQoNCb0JDQotCd0JXQpNCi0KzCu1zQkNCo0JDQm9Cs0KfQmNCd0KHQmtCe0JUg0JzQldCh0KLQntCg0J7QltCU0JXQndCY0JU/elements";





        public double retrieveValue(String wellPath, String parameterPath)
        {
            String objectName=parseWell(wellPath);
                String deviceName=parseDevice(parameterPath);
                String tagName = parseTag(wellPath, parameterPath);


            string jsonWithObjects;
            string jsonWithDevices;
            string jsonWithTags;
            string jsonWithTagAttributes;
            string jsonWithValue;


            WebClient properWebClient = getProperWebClient();

            jsonWithObjects = properWebClient.DownloadString(ServerAddr);
            string objectDevicesAddr = getObjectElementsAddr(jsonWithObjects, objectName);

            jsonWithDevices = properWebClient.DownloadString(objectDevicesAddr);
            string deviceTagsAddr = getObjectElementsAddr(jsonWithDevices, deviceName);

            jsonWithTags = properWebClient.DownloadString(deviceTagsAddr);
            string tagAttributesAddr = getObjectAttributesAddr(jsonWithTags, tagName);

            jsonWithTagAttributes = properWebClient.DownloadString(tagAttributesAddr);
            string tagValueAttributeAddr = getObjectValueAddr(jsonWithTagAttributes, "Value");


            jsonWithValue = properWebClient.DownloadString(tagValueAttributeAddr);
            double value = getValue(jsonWithValue);
            return value;

        }

        public double getValue(string jsonWithValue)
        {

            dynamic obj = JObject.Parse(jsonWithValue);
            double value = (double)obj.Value.Value;
            return value;

        }



        public string getObjectElementsAddr(string jsonWithObjects, string ObjectName)
        {
            string objectElementsAddr = "";
            PIObjectList objectsToSearch = new PIObjectList();
            objectsToSearch = JsonConvert.DeserializeObject<PIObjectList>(jsonWithObjects);
            int indexOfObjectWithThisName = findObjectWithNameInElements(objectsToSearch, ObjectName);
            if (indexOfObjectWithThisName != -1)
            {
                objectElementsAddr = objectsToSearch.Items[indexOfObjectWithThisName].Links.Elements;
            }
            return objectElementsAddr;

        }

        public string getObjectValueAddr(string jsonWithObjects, string ObjectName)
        {
            string objectElementsAddr = "";
            PIAttributeList objectsToSearch = new PIAttributeList();
            objectsToSearch = JsonConvert.DeserializeObject<PIAttributeList>(jsonWithObjects);
            int indexOfObjectWithThisName = findObjectWithNameInAttributes(objectsToSearch, ObjectName);
            if (indexOfObjectWithThisName != -1)
            {
                objectElementsAddr = objectsToSearch.Items[indexOfObjectWithThisName].Links.Value;
            }
            return objectElementsAddr;

        }

        public string getObjectAttributesAddr(string jsonWithObjects, string ObjectName)
        {
            string objectElementsAddr = "";
            PIAttributeList objectsToSearch = new PIAttributeList();
            objectsToSearch = JsonConvert.DeserializeObject<PIAttributeList>(jsonWithObjects);
            int indexOfObjectWithThisName = findObjectWithNameInAttributes(objectsToSearch, ObjectName);
            if (indexOfObjectWithThisName != -1)
            {
                objectElementsAddr = objectsToSearch.Items[indexOfObjectWithThisName].Links.Attributes;
            }
            return objectElementsAddr;

        }




        public int findObjectWithNameInAttributes(PIAttributeList objectsToSearch, string ObjectName)
        {
            int index = -1;
            Boolean notFound = true;
            for (int i = 0; (i < objectsToSearch.Items.Count) && notFound; i++)
            {
                if (ObjectName.Equals(objectsToSearch.Items[i].Name))
                {
                    index = i;
                    notFound = false;

                }
            }
            return index;
        }

        public int findObjectWithNameInElements(PIObjectList objectsToSearch, string ObjectName)
        {
            int index = -1;
            Boolean notFound = true;
            for (int i = 0; (i < objectsToSearch.Items.Count) && notFound; i++)
            {
                if (ObjectName.Equals(objectsToSearch.Items[i].Name))
                {
                    index = i;
                    notFound = false;

                }
            }
            return index;
        }

        public double parseValue(string jsonWithValue)
        {

            dynamic obj = JObject.Parse(jsonWithValue);
            double value = (double)obj.Value;
            return value;

        }




        String parseTag(String wellPath, String parameterPath)
        {
            String well = parseWell(wellPath);
            String device = parseDevice(parameterPath);
            String tag = wellPath + '.' + well + parameterPath.Substring(1);
            return tag;


        }

        public String parseWell(String wellPath)
        {
            char delimiter = '.';
            string[] tagHierarchyPoints = wellPath.Split(delimiter);
            string well = tagHierarchyPoints[tagHierarchyPoints.Length - 1];
            return well;
        }

        public String parseDevice(String parameterPath)
        {
            char delimiterForTag = '.';
            char delimiterForDevice = '_';
            string[] tagHierarchyPoints = parameterPath.Split(delimiterForTag);
            string wellWithDevice = tagHierarchyPoints[tagHierarchyPoints.Length - 2];
            string[] wellWithDevicePoints = wellWithDevice.Split(delimiterForDevice);
            string device = wellWithDevicePoints[wellWithDevicePoints.Length - 1];
            string deviceWithProperDevice = firstLetterUpperCase(device);
            return deviceWithProperDevice;
        }

        public String firstLetterUpperCase(string word)
        {

            if (string.IsNullOrEmpty(word))
            {
                return string.Empty;
            }
            string wordWithoutFirstLetter = word.Substring(1).ToLower();
            return char.ToUpper(word[0]) + wordWithoutFirstLetter;

        }

        public WebClient getProperWebClient()
        {
            WebClient client = new WebClient();
            IgnoreAllCertificates();
            client.Encoding = System.Text.Encoding.UTF8;
            return client;

        }

        public void IgnoreAllCertificates()
        {
            System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
        }
    }
}