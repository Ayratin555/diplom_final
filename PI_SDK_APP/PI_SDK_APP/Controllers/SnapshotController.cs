﻿using OSIsoft.AF;
using OSIsoft.AF.Asset;
using OSIsoft.AF.Time;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Diagnostics;
using PI_SDK_APP.Models;
using PI_SDK_APP.Services;

namespace PI_SDK_APP.Controllers
{
    public class SnapshotController : Controller


    {

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]

        public ActionResult GetSnapshotUsingPISDKHierarchy(String wellPath, String parameterPath)
        {
            Stopwatch stopWatch = Stopwatch.StartNew();

            ValueRetrieverHierarchy retriever = new ValueRetrieverHierarchy();
            double value = retriever.retrieveValue(wellPath, parameterPath);


            stopWatch.Stop();
            String time = "Выполнение  PI Web API Иерархия: " + stopWatch.ElapsedMilliseconds.ToString() + System.Environment.NewLine;
            System.Diagnostics.Debug.WriteLine(time);
            System.IO.File.AppendAllText(@"C:\Users\Ayrat\Documents\log2.txt", time);

            return this.Json(value, JsonRequestBehavior.AllowGet);
        }
 
        [HttpPost]

        public ActionResult GetSnapshotUsingPIWebAPI(String wellPath, String parameterPath)
        {
            Stopwatch stopWatch = Stopwatch.StartNew(); 

            ValueRetriever retriever = new ValueRetriever();
            double value = retriever.retrieveValue(wellPath, parameterPath);
            

            stopWatch.Stop();
            String time = "Выполнение  PI Web API: " + stopWatch.ElapsedMilliseconds.ToString() + System.Environment.NewLine;
            System.Diagnostics.Debug.WriteLine(time);
            System.IO.File.AppendAllText(@"C:\Users\Ayrat\Documents\log2.txt", time);

            return this.Json(value, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]

        public ActionResult GetSnapshotUsingPISDK(String wellPath, String parameterPath)
        {
            Stopwatch stopWatch = Stopwatch.StartNew(); 

            ValueRetrieverPISDK retriever = new ValueRetrieverPISDK();
            string value = retriever.retrieveValue(wellPath, parameterPath);
            

            stopWatch.Stop();
            String time = "Выполнение  PI SDK: " + stopWatch.ElapsedMilliseconds.ToString() + System.Environment.NewLine;
            System.Diagnostics.Debug.WriteLine(time);
            System.IO.File.AppendAllText(@"C:\Users\Ayrat\Documents\log2.txt", time);

            return this.Json(value, JsonRequestBehavior.AllowGet);
        }



    }
}
